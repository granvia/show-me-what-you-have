<div align="center">
    <h1>Show Me What You Have</h1>
    <h5> Who does not remember this ageless quote?</h5>
</div>

## What?

## Why?

## How?

## Milestones
Current and upcoming milestones (major features only):

### 1.0.0 - Stable
![Progress](http://progressed.io/bar/0)

**In Scope:**
  - Fully working and stable version ready for deploy.
  - Integration with git repository (Gitlab support).
  - Localization support.
  - Tags support.

**Out of Scope:**
  - Detailed preview in 3d.
  - Responsive layout.
  - Authenication service integration.
  - Displaying assets from more than one git repository in the same time.
  - Github and other Git likely services support.
  - Installation wizard.
  
**Milestones:**
- [ ] Backend service to listen git repository for pushes to main branch.
- [ ] Converting 3d assets to .glTF (GL Transmission Format).
  - [ ] Running Blender in headless mode.
  - [ ] Execute python script for Blender to convert Gothic 3D assets to .glTF extenstion.
- [ ] Implement REST API service for backend-frontend integration.
- [ ] Setup MongoDB for storing assets metadata.
- [ ] Tags support by backend.
- [ ] Setup CI process + Docker support.
- [ ] React based frontend
  - [ ] UI
  - [ ] Rendering scenes based on particular configuration + storing them in the backend
  - [ ] Detailed assets preview with parameters (used textured, functions etc.)
  - [ ] Setting, removing tags for assets
  - [ ] Copying to clipboard asset's name by one click

